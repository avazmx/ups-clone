import { Component, OnInit } from '@angular/core';
import { headerMenu } from '../../shared/models/header-menu';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  options = headerMenu;

  constructor() { }

  ngOnInit() {
  }

}
