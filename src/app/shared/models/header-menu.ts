export const headerMenu = [
  {
    description: 'Home',
    name: 'Home'
  },
  {
    description: 'Tracking',
    name: 'Tracking'
  },
  {
    description: 'Shipping',
    name: 'Shipping'
  },
  {
    description: 'Services',
    name: 'Services'
  }
];
